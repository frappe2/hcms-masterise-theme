export function buildUI(arrayTabFunction) {
    var content = ''
    arrayTabFunction.forEach((element, index) => {
        content.append(
            `<div class="tab" id="tab-${index}" data-rel="1">${element.name}</div>`
        )
    })

    var renderHTML = `
        <div class="tabs--wrapper" >
        <div class="tab-indicator"></div>
        ${content}
        </div >
    `
    return renderHTML
}