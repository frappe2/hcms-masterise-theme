from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in hcms_masterise_theme/__init__.py
from hcms_masterise_theme import __version__ as version

setup(
	name="hcms_masterise_theme",
	version=version,
	description="Business Theme for ERPNext / Frappe",
	author="Khanh Le",
	author_email="vankhanh.le@masterisehomes.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
